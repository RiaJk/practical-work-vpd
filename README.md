# Practical work 5

## Modules:
* lagrange
* check_input
* main

## lagrange

Contain function __lagrange()__ that gets two numbers - interval boundaries and that calculates numbers satisfying the Lagrange condition on this interval.

## check_input

Contain function __check_input__ that get two arguments which type function check. If type of bith arguments is integer - return tuple of two arguments. Else - print _'ValueError: ведите 2 целых числа.'_

## main

Contain menu.
